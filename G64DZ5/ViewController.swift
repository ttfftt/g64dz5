//
//  ViewController.swift
//  G64DZ5
//
//  Created by mac on 7/19/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let phrase: NSMutableString = "ЯЗЗЬ"
        let massiv: Array = ["lada", "sedan", "baklazhan"]
        zadanie7()//Задача 7. Сортировка массива не встроенным методом по возрастанию + удалить дубликаты/встроенный метод
        zadanie7New()//Задача 7. Сортировка массива не встроенным методом по возрастанию + удалить дубликаты/ не встроенный метод
        zadanie8(phrase:(phrase))
        zadanie88()
        zadanie9(massiv:(massiv))
        //        zadanie10()
        drawBox()
    }
    
    //    Задача 7. Сортировка массива не встроенным методом по возрастанию + удалить дубликаты
    func zadanie7() { //встроенный метод
        var massiv = [9, 1, 2, 5, 1, 7]
        massiv.remove(at: 1)
        print(massiv.sorted())
    }
    
    //    Задача 7. Сортировка массива не встроенным методом по возрастанию + удалить дубликаты/ не встроенный метод
    func zadanie7New() { //не встроенный метод
        var massiv = [9, 1, 2, 5, 1, 7]
        massiv.remove(at: 1)
        let nine = massiv [0]
        let two = massiv [1]
        let five = massiv [2]
        let one = massiv [3]
        let seven = massiv [4]
        let massivSorted = [one, two, five, seven, nine]
        print(massivSorted)
    }
    
    //    Задача 8. Написать метод, который будет переводить строку в транслит.
    func zadanie8(phrase: NSMutableString) {
        let y = phrase
        let range = NSRange(location: 0, length: 4)
        phrase.insert("YAZZ", at: 0)
        print(y.substring(with: range))
    }
    
    //    Задача 8. Написать метод, который будет переводить строку в транслит. (Еще один вариант решения)
    func zadanie88() {
        var frasa = "ЯЗЗЬ"
        for _ in frasa.indices {
            if frasa.contains("Я") {
                frasa.remove(at: frasa.index(frasa.startIndex, offsetBy: 0))
                frasa.insert("Y", at: frasa.index(frasa.startIndex, offsetBy: 0))
            }
            if frasa.contains("З") {
                frasa.remove(at: frasa.index(frasa.startIndex, offsetBy: 1))
                frasa.insert("A", at: frasa.index(frasa.startIndex, offsetBy: 1))
            }
            
            if frasa.contains("З") {
                frasa.remove(at: frasa.index(frasa.startIndex, offsetBy: 2))
                frasa.insert("Z", at: frasa.index(frasa.startIndex, offsetBy: 2))
            }
            
            if frasa.contains("Ь") {
                frasa.remove(at: frasa.index(frasa.startIndex, offsetBy: 3))
                frasa.insert("Z", at: frasa.index(frasa.startIndex, offsetBy: 3))
            }
            
        }
        
        print(frasa)
    }
    
    
    //    Задача 9. Сделать выборку из массива строк в которых содержится указанная строка
    func zadanie9(massiv: Array <String>) {
        let first1 = massiv[0]
        let first2 = massiv[1]
        let first3 = massiv[2]
        if first1.contains("da") {
            print(first1)
        }
        if first2.contains("da") {
            print(first2)
        }
        if first3.contains("da") {
            print(first3)
        }
    }
    
    //    Задача 10. Set<String> - antimat [“fuck”, “fak”] “hello my fak” “hello my ***”
    //    func zadanie10() {
    
    
    func drawBox() {
        let rect = CGRect.init(x: 10, y: 100, width: 32, height: 32)
        let rect1 = CGRect.init(x: 52, y: 100, width: 32, height: 32)
        let rect2 = CGRect.init(x: 94, y: 100, width: 32, height: 32)
        let box = UIView(frame: rect)
        let color = UIColor.blue
        box.backgroundColor = color
        view.addSubview(box)
        let box1 = UIView(frame: rect1)
        box1.backgroundColor = color
        view.addSubview(box1)
        let box2 = UIView(frame: rect2)
        box2.backgroundColor = color
        view.addSubview(box2)
        
        
    }
    
}





